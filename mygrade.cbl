       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MYGRADE.
       AUTHOR. THANTHAT

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           SELECT MY-GRADE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT SUM-AVG ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION.
       FD  MY-GRADE.
       01 GRADE-DETAILS.
          88 END-OF-MY-GRADE              VALUE HIGH-VALUE. 
       05 SUB-CODE            PIC   X(6).
          05 SUB-NAME         PIC   X(50).
          05 CREDIT           PIC   9.
          05 GRADE            PIC   99V99.
          05 SCI-CREDIT       PIC 9.
          05 CS-CREDIT        PIC 9.
          05 SCI-GRADE        PIC 99V99.
          05 CS-GRADE         PIC 99V99.
       FD SUM-AVG.
          01 AVG             PIC   9V999.
          01 AVG-SCI         PIC 9V999.
          01 AVG-CS          PIC 9V999.

       WORKING-STORAGE SECTION. 
       01 RESULT-SUM-GRADE    PIC 999V999  VALUE ZEROES.
       01 RESULT-SUM-CREDIT   PIC 999V999  VALUE ZEROES.
       01 RESULT-AVG-GRADE    PIC 9V999  VALUE ZEROES.
       01 RESULT-SCI-GRADE    PIC 999V999  VALUE ZEROES.
       01 RESULT-CS-GRADE     PIC 999V999  VALUE ZEROES.
       01 RESULT-SCI-CREDIT   PIC 999V999  VALUE ZEROES.
       01 RESULT-CS-CREDIT    PIC 999V999  VALUE ZEROES.
       01 RESULT-SCI-AVG      PIC 9V999  VALUE ZEROES.
       01 RESULT-CS-AVG       PIC 9V999   VALUE ZEROES.


       PROCEDURE DIVISION .
       000-BEGIN.
           OPEN INPUT MY-GRADE
           OPEN OUTPUT SUM-AVG 
           PERFORM UNTIL END-OF-MY-GRADE 
                   READ MY-GRADE
                   AT END
                      SET END-OF-MY-GRADE TO TRUE
                   END-READ
                   IF NOT END-OF-MY-GRADE THEN 
                      IF GRADE = "A"
                         COMPUTE RESULT-SUM-GRADE = RESULT-SUM-GRADE +
                            (CREDIT * 4)
                         COMPUTE RESULT-SUM-CREDIT =RESULT-SUM-CREDIT +
      -                  CREDIT
                         IF (SUB-CODE(1:1) = 3)
                            COMPUTE RESULT-SCI-GRADE = RESULT-SCI-GRADE
                               +
                               (CREDIT * 4)
                          COMPUTE RESULT-SCI-CREDIT =RESULT-SCI-CREDIT +
      -                  CREDIT
                         END-IF 
                         IF (SUB-CODE(1:2) = 31)
                            COMPUTE RESULT-CS-GRADE = RESULT-CS-GRADE +
                               (CREDIT * 4)
                          COMPUTE RESULT-CS-CREDIT =RESULT-CS-CREDIT +
      -                  CREDIT
                         END-IF
                          

                      END-IF 
                      IF GRADE = "B+"
                         COMPUTE RESULT-SUM-GRADE = RESULT-SUM-GRADE +
                            (CREDIT * 3.5)
                         COMPUTE RESULT-SUM-CREDIT =RESULT-SUM-CREDIT +
      -                  CREDIT
                         IF (SUB-CODE(1:1) = 3)
                            COMPUTE RESULT-SCI-GRADE = RESULT-SCI-GRADE
                               +
                               (CREDIT * 3.5)
                          COMPUTE RESULT-SCI-CREDIT =RESULT-SCI-CREDIT +
      -                  CREDIT
                         END-IF 
                         IF (SUB-CODE(1:2) = 31)
                            COMPUTE RESULT-CS-GRADE = RESULT-CS-GRADE +
                               (CREDIT * 3.5)
                          COMPUTE RESULT-CS-CREDIT =RESULT-CS-CREDIT +
      -                  CREDIT
                         END-IF
                      END-IF 
                      IF GRADE = "B"
                         COMPUTE RESULT-SUM-GRADE = RESULT-SUM-GRADE +
                            (CREDIT * 3)
                         COMPUTE RESULT-SUM-CREDIT =RESULT-SUM-CREDIT +
      -                  CREDIT
                         IF (SUB-CODE(1:1) = 3)
                            COMPUTE RESULT-SCI-GRADE = RESULT-SCI-GRADE
                               +
                               (CREDIT * 3)
                          COMPUTE RESULT-SCI-CREDIT =RESULT-SCI-CREDIT +
      -                  CREDIT
                         END-IF 
                         IF (SUB-CODE(1:2) = 31)
                            COMPUTE RESULT-CS-GRADE = RESULT-CS-GRADE +
                               (CREDIT * 3)
                          COMPUTE RESULT-CS-CREDIT =RESULT-CS-CREDIT +
      -                  CREDIT
                         END-IF
                      END-IF 
                      IF GRADE = "C+"
                         COMPUTE RESULT-SUM-GRADE = RESULT-SUM-GRADE +
                            (CREDIT * 2.5)
                         COMPUTE RESULT-SUM-CREDIT =RESULT-SUM-CREDIT +
      -                  CREDIT
                         IF (SUB-CODE(1:1) = 3)
                            COMPUTE RESULT-SCI-GRADE = RESULT-SCI-GRADE
                               +
                               (CREDIT * 2.5)
                          COMPUTE RESULT-SCI-CREDIT =RESULT-SCI-CREDIT +
      -                  CREDIT
                         END-IF 
                         IF (SUB-CODE(1:2) = 31)
                            COMPUTE RESULT-CS-GRADE = RESULT-CS-GRADE +
                               (CREDIT * 2.5)
                          COMPUTE RESULT-CS-CREDIT =RESULT-CS-CREDIT +
      -                  CREDIT
                         END-IF
                      END-IF 
                      IF GRADE = "C"
                         COMPUTE RESULT-SUM-GRADE = RESULT-SUM-GRADE +
                            (CREDIT * 2)
                         COMPUTE RESULT-SUM-CREDIT =RESULT-SUM-CREDIT +
      -                  CREDIT
                         IF (SUB-CODE(1:1) = 3)
                            COMPUTE RESULT-SCI-GRADE = RESULT-SCI-GRADE
                               +
                               (CREDIT * 2)
                          COMPUTE RESULT-SCI-CREDIT =RESULT-SCI-CREDIT +
      -                  CREDIT
                         END-IF 
                         IF (SUB-CODE(1:2) = 31)
                            COMPUTE RESULT-CS-GRADE = RESULT-CS-GRADE +
                               (CREDIT * 2)
                          COMPUTE RESULT-CS-CREDIT =RESULT-CS-CREDIT +
      -                  CREDIT
                         END-IF
                      END-IF 
                      IF GRADE = "D+"
                         COMPUTE RESULT-SUM-GRADE = RESULT-SUM-GRADE +
                            (CREDIT * 1.5)
                         COMPUTE RESULT-SUM-CREDIT =RESULT-SUM-CREDIT +
      -                  CREDIT
                         IF (SUB-CODE(1:1) = 3)
                            COMPUTE RESULT-SCI-GRADE = RESULT-SCI-GRADE
                               +
                               (CREDIT * 1.5)
                          COMPUTE RESULT-SCI-CREDIT =RESULT-SCI-CREDIT +
      -                  CREDIT
                         END-IF 
                         IF (SUB-CODE(1:2) = 31)
                            COMPUTE RESULT-CS-GRADE = RESULT-CS-GRADE +
                               (CREDIT * 1.5)
                          COMPUTE RESULT-CS-CREDIT =RESULT-CS-CREDIT +
      -                  CREDIT
                         END-IF
                      END-IF 
                      IF GRADE = "D"
                         COMPUTE RESULT-SUM-GRADE = RESULT-SUM-GRADE +
                            (CREDIT * 1)
                         COMPUTE RESULT-SUM-CREDIT =RESULT-SUM-CREDIT +
      -                  CREDIT
                         IF (SUB-CODE(1:1) = 3)
                            COMPUTE RESULT-SCI-GRADE = RESULT-SCI-GRADE
                               +
                               (CREDIT * 1)
                          COMPUTE RESULT-SCI-CREDIT =RESULT-SCI-CREDIT +
      -                  CREDIT
                         END-IF 
                         IF (SUB-CODE(1:2) = 31)
                            COMPUTE RESULT-CS-GRADE = RESULT-CS-GRADE +
                               (CREDIT * 1)
                          COMPUTE RESULT-CS-CREDIT =RESULT-CS-CREDIT +
      -                  CREDIT
                         END-IF
                      END-IF 
                      IF GRADE = "F"
                         COMPUTE RESULT-SUM-GRADE = RESULT-SUM-GRADE +
                            (CREDIT * 0)
                         COMPUTE RESULT-SUM-CREDIT =RESULT-SUM-CREDIT +
      -                  CREDIT 
                         IF (SUB-CODE(1:1) = 3)
                            COMPUTE RESULT-SCI-GRADE = RESULT-SCI-GRADE
                               +
                               (CREDIT * 0)
                          COMPUTE RESULT-SCI-CREDIT =RESULT-SCI-CREDIT +
      -                  CREDIT
                         END-IF 
                         IF (SUB-CODE(1:2) = 31)
                            COMPUTE RESULT-CS-GRADE = RESULT-CS-GRADE +
                               (CREDIT * 0)
                          COMPUTE RESULT-CS-CREDIT =RESULT-CS-CREDIT +
      -                  CREDIT
                         END-IF
                      END-IF 
                   END-IF 
      

           
           END-PERFORM

           COMPUTE RESULT-AVG-GRADE = RESULT-SUM-GRADE /
              RESULT-SUM-CREDIT
           DISPLAY "RESULT-AVG-GRADE: " RESULT-AVG-GRADE  
           COMPUTE RESULT-SCI-AVG = RESULT-SCI-GRADE /RESULT-SCI-CREDIT 
           
           DISPLAY "RESULT-AVG-SCI:" RESULT-SCI-AVG
           COMPUTE RESULT-CS-AVG = RESULT-CS-GRADE /RESULT-CS-CREDIT
           DISPLAY "RESULT-CS-AVG: " RESULT-CS-AVG
           MOVE RESULT-AVG-GRADE TO AVG
           WRITE AVG
           MOVE RESULT-SCI-AVG   TO AVG-SCI
           WRITE AVG-SCI 
           MOVE RESULT-CS-AVG TO AVG-CS 
           WRITE AVG-CS 
           CLOSE SUM-AVG 
           CLOSE MY-GRADE 
           GOBACK 
           .

